package stubtest;


import com.jayway.restassured.path.xml.XmlPath;
import com.jayway.restassured.path.xml.element.Node;
import groovy.util.NodeList;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;
import utils.JmsClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.jayway.restassured.path.xml.XmlPath.from;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * Created by sbt-yakimov-vi on 19.10.2016.
 */
@Features("Тестирование интеграции")
@Stories("Тестирование сервиса получения списка клиентов")
public class getLegalClientListTest {

    private JmsClient jmsClient = new JmsClient();
    private String filename = "GetLegalClientListRq";
    private XmlPath xmlResponse;
    private XmlPath xmlRequest;
    String xPathClientList = "GetLegalClientListRs.ListOfAccountInfo.ClientCardInfo";



    //ГовноКод TODO: Сделать EmployeeCode + его по справочнику + негативные сценарии
    @Title("Проверка атрибутивного состава клиентов + логика по справочникам(Переделать)")
    @Test
    public void checkClientAttribute(){

        String accountId;
        String orgType;
        String employerCode;

        Integer clientList=  xmlResponse.getList(xPathClientList).size();
        for (int i = 0; i < clientList; i ++){
//            String mask = ("[" + i + "]");

            accountId = xmlResponse.get(xPathClientList + "[" + i + "]" + ".AccountId").toString();
            orgType = xmlResponse.get(xPathClientList + "[" + i + "]" + ".OrgRec.OrgId.OrgType").toString();
            employerCode = xmlResponse.get("GetLegalClientListRs.ListOfAccountInfo.ClientCardInfo[0].OrgRec.OrgInfo").toString();


            assertTrue(accountId != "" || accountId != null,"Неопределенный AccountId : " + accountId);
            assertEquals(orgType, "MDMId","Неверный OrgType: " + orgType);
//            assertTrue(employerCode == "Account" || employerCode == "Branch" || employerCode == "Holding" ,"Неверный EmployeeCode: " + employerCode);




        }

    }

}
