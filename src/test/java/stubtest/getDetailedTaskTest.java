package stubtest;

import com.jayway.restassured.path.xml.XmlPath;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import utils.JmsClient;

import static com.jayway.restassured.path.xml.XmlPath.from;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;


/**
 * Created by victo on 16.10.2016.
 */
@Features("Тестирование интеграции")
@Stories("Тестирование сервиса получениядетальной информации по карточке")
public class getDetailedTaskTest {

    private String requestRoot = "GetLegalClientManagerTaskDetailsRq";
    private String responseRoot = "GetLegalClientManagerTaskDetailsRs";
    private String teamInfo = responseRoot + ".ActionInfoContactCenter.ListOfTeamInfo.TeamInfo";
    private String personInfo = teamInfo + ".PersonInfo";
    private XmlPath xmlResponse;
    private XmlPath xmlRequest;

    JmsClient jmsClient  = new JmsClient();


    @BeforeClass
    public void sendAndRead(){
        String request = jmsClient.send("GetLegalClientManagerTaskDetailsRequest");
        xmlRequest = from(request);
        String response = jmsClient.read();
        xmlResponse = from(response);
    }



    @Test
    public void checkRequestAttribute(){

        assertEquals(xmlRequest.get(requestRoot + ".Method"), "IANUAERMGetActionDetails", " Неверный метод в запросе");
        assertEquals(xmlRequest.get(requestRoot + ".SPName"), "urn:sbrfsystems:99-ufs", " Неверное имя системы в запросе");
        assertEquals(xmlRequest.get(requestRoot + ".SystemId"),"urn:sbrfsystems:99-crmorg", " Неверный айди системы в запросе");

    }

    @Test
    public void checkRqUID(){

        assertEquals(xmlResponse.get(responseRoot  + ".RqUID"), xmlRequest.get(requestRoot + ".RqUID"),"RqId запроса и ответа не равны");

    }

    @Test
    public void checkDescription(){

        assertNotEquals(xmlResponse.get(responseRoot  + ".Description"),null,"Desription равен null");

    }

    ///TO DO
//    @Test
//    public void checkMethod(){
//
//        assertEquals(xmlResponse.get(responseRoot  + ".Method"),"IANUAERMGetActionDetails","Неверный метод");
//
//    }

    @Test
    public void checkActivityStatus(){

        assertEquals(xmlResponse.get(responseRoot + ".ActionInfoContactCenter.ActivityStatus"),"Done","Статус запроса не равен Done");

    }

    //Нормальный статус код узнать!!
    @Test
    public void checkStatusCode(){

        assertNotEquals(xmlResponse.get(responseRoot  + ".StatusCode"),null,"Статус код не определен");

    }

    //TO DO Переделать на Sudir
    @Test
    public void checkUserLogin(){

        assertEquals(xmlResponse.get(teamInfo  + ".UserLogin"),"Ivanov"," UserLogin != Ivanov");

    }

    @Test
    public void checkPersonInfo(){

        assertEquals(xmlResponse.get(personInfo + ".FullName"),"Иванов Иван Иванович","Неверное полные данные о представителе");

    }

    @Test
    public void checkPersonFIO(){

        assertEquals(xmlResponse.get(personInfo + ".PersonName.LastName"),"Иванов","Неверноая фамилия представителя");
        assertEquals(xmlResponse.get(personInfo + ".PersonName.FirstName"),"Иван","Неверноая фамилия представителя");
        assertEquals(xmlResponse.get(personInfo + ".PersonName.MiddleName"),"Иванович","Неверноая фамилия представителя");

    }








}
