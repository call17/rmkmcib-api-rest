package test;



import basestep.TasksStep;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;


import static utils.JsonUtils.*;

/**
 * Created by sbt-yakimov-vi on 27.10.2016.
 */

// TODO: 28.10.2016 Редактирование, создание карточки + База знаний
// TODO: 28.10.2016 Проапдейтить карточку задачи, и на тот же манер карточку клиента

public class TaskTest extends TasksStep{


    private Integer page = 1;
    private String taskId = "1000010";
    private String idNewTask;
    private String updatedTaskId;

    private String jsonCreateTask = readjsonFromFile("\\src\\test\\resources\\testdata\\createTask.json");
    private String jsonUpdateTask = readjsonFromFile("\\src\\test\\resources\\testdata\\updateTask.json");


    @Features("Сервисы интеграции")
    @Stories("Проверка сервиса GetLegalClientManagerTaskList")
    @TestCaseId("MSV")
    @Test
    public void checkGetLegalClientManagerTaskList(){

        String xsdSchemaClassPath = "\\xml\\SrvGetLegalClientManagerTaskList002.xsd";
        String xmlNodeRs = "GetLegalClientManagerTaskListRs";
        String xmlNodeRq = "GetLegalClientManagerTaskListRq";

        authClientManager();
        sendRequestTaskList(page);
        checkRqInLog(xmlNodeRq);
        validateRq(xsdSchemaClassPath);
        checkRsInLog(xmlNodeRs);
        validateRs(xsdSchemaClassPath);
        checkSPNameInReq();
        checkSystemIdInReq();
        checkMethodInRq();
        checkUserLoginInRq();
        checkResponse();

    }

    @Features("Сервисы интеграции")
    @Stories("Проверка сервиса GetLegalClientManagerDetails")
    @TestCaseId("MSV")
    @Test
    public void checkGetLegalClientManagerTaskDetails(){

        String xsdSchemaClassPath = "\\xml\\SrvGetLegalClientManagerTaskDetails002.xsd";
        String xmlNodeRs = "GetLegalClientManagerTaskDetailsRs";
        String xmlNodeRq = "GetLegalClientManagerTaskDetailsRq";

        authClientManager();
        sendRequestTaskCard(taskId);
        checkRqInLog(xmlNodeRq);
        validateRq(xsdSchemaClassPath);
        checkRqInLog(xmlNodeRs);
        checkSPNameInReq();
        checkSystemIdInReq();
        checkMethodInRq();
        checkUserLoginInRq();
        checkResponse();
    }


    @Features("Тестирование рест-сервиса получения Списка Задач")
    @Stories("Наличие заданного атрибутивного состава Списка задач")
    @TestCaseId("4.5_1, 4.5_2")
    @Test
    public void checkAttributeTaskList(){

        authClientManager();
        checkAttributeTaskList(page);

    }

    @Features("Тестирование рест-сервиса получения карточки задачи")
    @Stories("Наличие заданного атрибута карточки Задач")
    @TestCaseId("7.3_1")
    @Test
    public void checkAttributeTaskCard(){

        authClientManager();
        checkAttributeInTaskCard(taskId);

    }

    // TODO: 01.11.2016 Разобраться с получением вновь созданной задачи
    
    @Features("Тестирование рест-сервиса создания задачи")
    @Stories("Создание новой задачи, проверка атрибутивного состава созданной задачи, проверка, что созданная задача отображаетсяв  списке задач")
    @TestCaseId("7.12??")
    @Test
    public void checkNewTask(){

        authClientManager();
        idNewTask = createTask(jsonCreateTask);
        getTask(idNewTask);
        checkTaskListContainsTaskWithId(idNewTask);

    }

    @Features("Тестирование рест-сервиса редактирования задачи")
    @Stories("Реадктирование задачи")
    @TestCaseId("???")
    @Test
    public void checkUpdateTask(){

        authClientManager();
        updatedTaskId = updateTask(jsonUpdateTask,idNewTask);
        getTask(updatedTaskId);
        checkTaskListContainsTaskWithId(updatedTaskId);

    }
























    //Украл у Иры
//    public void checkCurrentTaskList() {
//        String response = fabric.getTasks("current").then().statusCode(200).body("success", equalTo(true))
//                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("tasks.json")).extract().asString();
//        List<String> count = parse(response)
//                .read("$.body[*][?(@.status.code in ['In Progress'] && @.plannedStartDate=~/" + fabric.getCurrentDate()
//                        + ".*?/i)].id");
//
//        List<String> taskCount = parse(response).read("$.body[*].id");
//
//        Assert.assertTrue(taskCount.size() == count.size());
//    }
}
