package test;

import basestep.ClientsStep;

import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;


// TODO: 28.10.2016 В соапе сотались 4.25.5,4.25.10, 4.25.8
// TODO: 01.11.2016 Отладить, после фикса списка клиентов !!!!

/**
 * Created by sbt-yakimov-vi on 27.10.2016.
 */
public class ClientTest extends ClientsStep{


    private String page = "1";
    private String pageSize = "1";
    private String clientId = "1-5RG5G";
    private String expectedUserLogin = "testcrmmanager1";

    @Features("Сервисы интеграции")
    @Stories("Проверка сервиса GetLegalClientProfile")
    @TestCaseId("MSV")
    @Test
    public void checkGetLegalClientProfile(){

        String xsdSchemaClassPath = "xml\\SrvGetLegalClientProfile007.xsd";
        String expectedCardMethodName = "MRMCIBGetClientInfo";
        String xmlNodeCardRq = "GetLegalClientProfileRq";
        String xmlNodeCardRs = "GetLegalClientProfileRs";

        authClientManager();
        sendRequestClientCard(clientId);
        checkLogRq(xmlNodeCardRq);
        validateRq(xsdSchemaClassPath);
        checkLogRs(xmlNodeCardRs);
        validateRs(xsdSchemaClassPath);
        checkSystemIdInReq();
        checkSPNameInReq();
        checkMethodInRq(expectedCardMethodName);
        checkUserLoginInRq(expectedUserLogin);
        checkResponseClientCard();
    }

    @Features("Сервисы интеграции")
    @Stories("Проверка сервиса GetLegalClientList")
    @TestCaseId("MSV")
    @Test
    public void checkGetLegalClientList(){

        String xsdSchemaClassPath = "\\xml\\SrvGetLegalClientList005_28102016.xsd";
        String xmlNodeListRq = "GetLegalClientProfileRq";
        String xmlNodeListRs = "GetLegalClientProfileRs";
        String expectedListMethodName = "MRMCIBGetClientInfo";

        authClientManager();
        sendRequestClientList(page,pageSize);
        checkLogRq(xmlNodeListRq);
        validateRq(xsdSchemaClassPath);
        checkLogRs(xmlNodeListRs);
        validateRs(xsdSchemaClassPath);
        checkSystemIdInReq();
        checkSPNameInReq();
        checkMethodInRq(expectedListMethodName);
        checkUserLoginInRq(expectedUserLogin);
        checkResponseClientList();


    }


    @Features("Тестирование рест-сервиса получения карточки клиета")
    @Stories("Наличие заданного атрибутного состава Карточки клиента")
    @TestCaseId("4.25.1")
    @Test
    public void attributeClientCardTest(){

        authClientManager();
        checkAttributeClientCard(clientId);

    }

    // TODO: 01.11.2016 Пушить разрабов с фиксом

    @Features("Тестирование рест-сервиса получения списка клиентов")
    @Stories("Наличие атррибутивного состава в списке клиентов")
    @TestCaseId("4.19.6,4.19.1")
    @Test
    public void attributeClientListTest(){

        authClientManager();
        checkAttributeInClientList(page,pageSize);
    }

    // TODO: 28.10.2016 Добавить степы приобнолении ролевой модели

    @Features("Тестирование рест-сервиса получения списка клиентов")
    @Stories("Заполнение атрибута <Моя роль>(!!! only Step 4)")
    @TestCaseId("4.19.7")
    @Test
    public void roleWithDifferentAuth(){

        checkRoleWIthAuthClientManager(page, pageSize, "CAM");
    }

    @Features("Тестирование рест-сервиса получения списка клиентов")
    @Stories("Отсутствие клиентов в списке клиентов(!! Only Step 5)")
    @TestCaseId("4.19.3")
    @Test
    public void roleWithBadAuth(){

        checkClientListWithBadAuth(page,pageSize);
    }

    @Features("Тестирование рест-сервиса получения карточки клиента")
    @Stories("Наличие иерархического списка родительских организация")
    @TestCaseId("4.25.3")
    @Test
    public void checkIerarchyListInClientCard(){

        authClientManager();
        checkIerarchyContainsInClientCard(page,pageSize);
    }





}
