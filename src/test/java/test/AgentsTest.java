package test;

import basestep.AgentsStep;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

/**
 * Created by victo on 01.11.2016.
 */
public class AgentsTest extends AgentsStep {

    private String clientId = "1-5RG5G";

    @Features("Тестирование сервиса получения информации о представителе")
    @Stories("Проверка аттрибутивного состава информации о пердставителе")
    @TestCaseId("asd")
    @Test
    public void checkAttributeInAgentsCard(){

        authClientManager();
        getAgentsList(clientId);

    }

}
