package test;

import basestep.ProductsStep;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

/**
 * Created by victo on 25.10.2016.
 */
// TODO: 28.10.2016 Следить за json схемами
// TODO: 11.11.2016  Прогнать при новой поставке по Аквайрингу+ учтонить, будет ли ДБО, УДБО.


public class ProductsTest extends ProductsStep {

    private String orgId =  "1-15000Z";


    @Features("Сервисы интеграции")
    @Stories("Проверка сервиса GetSettlementAgreemt")
    @TestCaseId("MSV")
    @Test
    public void checkGetSettlementAgreemt(){

        String xsdSchemaClassPath = "\\xml\\SrvGetSettlementAgreemt.xsd";
        String xmlNodeRs = "GetSettlementAgreemtRs";
        String xmlNodeRq = "GetSettlementAgreemtRq";

        auth();
        sendRequestRKO(orgId);
        checkRqInLog(xmlNodeRq);
        validateRq(xsdSchemaClassPath);
        checkRqInLog(xmlNodeRs);
        validateRs(xsdSchemaClassPath);
        checkSPNameInReq();
        checkSystemIdInReq();
        checkOrgNum(orgId);
//        checkRKO(orgId);

    }

    @Features("Сервисы интеграции")
    @Stories("Проверка сервиса GetLegalDepositInfo")
    @TestCaseId("MSV")
    @Test
    public void checkGetLegalDepositInfo(){

        String xsdSchemaClassPath = "\\xml\\SrvGetLegalDepositInfo.xsd";
        String xmlNodeRs = "GetLegalDepositInfoRs";
        String xmlNodeRq = "GetLegalDepositInfoRq";

        auth();
        sendRequestDepositNsoSdo(orgId);
        checkRqInLog(xmlNodeRq);
        validateRq(xsdSchemaClassPath);
        checkRqInLog(xmlNodeRs);
        validateRs(xsdSchemaClassPath);
        checkSPNameInReq();
        checkSystemIdInReq();
        checkOrgNum(orgId);
//        checkDeposits(orgId);

    }

    @Features("Сервисы интеграции")
    @Stories("Проверка сервиса CredContractInfo")
    @TestCaseId("MSV")
    @Test
    public void checkCredContractInfo(){

        String xsdSchemaClassPath = "\\xml\\SrvCredContractInfo.xsd";
        String xmlNodeRs = "CredContractInfoRs";
        String xmlNodeRq = "CredContractInfoRq";

        auth();
        sendRequestCredits(orgId);
        checkRqInLog(xmlNodeRq);
        validateRq(xsdSchemaClassPath);
        checkRqInLog(xmlNodeRs);
        validateRs(xsdSchemaClassPath);
        checkSPNameInReq();
        checkSystemIdInReq();
        checkOrgNum(orgId);
//        checkCredits(orgId);

    }

    @Features("Сервисы интеграции")
    @Stories("Проверка сервиса GetCorporateCard")
    @TestCaseId("MSV")
    @Test
    public void checkGetCorporateCard(){

        String xsdSchemaClassPath = "\\xml\\SrvGetCorporateCard.xsd";
        String xmlNodeRs = "GetCorporateCardRs";
        String xmlNodeRq = "GetCorporateCardRq";

        auth();
        sendRequestCorporateCard(orgId);
        checkRqInLog(xmlNodeRq);
        validateRq(xsdSchemaClassPath);
        checkRqInLog(xmlNodeRs);
        validateRs(xsdSchemaClassPath);
        checkSPNameInReq();
        checkSystemIdInReq();
        checkOrgNum(orgId);
//        checkCredits(orgId);

    }


    @Features("Проверка атрибутивного состава продуктов")
    @Stories(" Наличие заданного аттрибутивного состава продуктов")
    @TestCaseId("4.10_1, 4.10_7 ")
    @Test
    public void checkAttribute(){

        auth();
        checkRKO(orgId);
        checkAttributeRKO();
        checkEncashment(orgId);
        checkAttributeEncashments();
        checkCorporateCards(orgId);
        checkAttributeCorporateCards();
        checkCredits(orgId);
        checkAttributeCredits();
        checkDeposits(orgId);
        checkAttributeDeposits();


    }


}
