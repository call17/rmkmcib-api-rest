package utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static utils.JSchClient.updateLog;


// TODO: 07.11.2016 Добавить логгер

/**
 * Created by sbt-yakimov-vi on 03.11.2016.
 */
public class LogsParser {

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/d/yy HH:mm:ss:SSS");
    private LocalDateTime responseTime;
    private LocalDateTime minRangeTime;
    private LocalDateTime maxRangeTime;
    private String tempXML;
    private String xmlRqFromLogs;
    private String xmlRsFromLogs;
    private Integer day;
    private Integer month;
    private Integer year;
    private Pattern regexp;

    public void setResponseTime(LocalDateTime responseTime){

        this.responseTime = responseTime;
        setRangeForParseLog(responseTime);
    }


    protected Boolean checkRqInLogs(String xmlNode)  {


        //Какой то говнокод, надо отрефакторить
        updateLog();


        if (responseTime != null){

            System.out.println("Время запроса: " + responseTime + "\n");
            setRangeForParseLog(responseTime);
        }else {

            System.out.println("Время запроса не установлено");
            throw new NullPointerException();
        }

        System.out.println("\n " + "Инициирован поиск запроса"+ "\n");
        regexp = Pattern.compile("(" + month + "/" + day + "/" + year + ".*[\\d][\\d]:[\\d][\\d]:[\\d][\\d]:[\\d][\\d][\\d]).*SENT.*(..<?xml.*" + xmlNode +".*" + ")");

        xmlRqFromLogs = findStringInLogs(regexp);
        if (xmlRqFromLogs.equals(null)){
            return false;
        }
        System.out.println("Что в лога RQ:" + xmlRqFromLogs);

        return true;
    }

    protected Boolean checkRsInLogs(String xmlNode){
        System.out.println("\n " + "Инициирован поиск ответа"+ "\n");
        regexp = Pattern.compile("(" + month + "/" + day + "/" + year + ".*[\\d][\\d]:[\\d][\\d]:[\\d][\\d]:[\\d][\\d][\\d]).*RECEIVED FROM [MQ|CACHE].*(..<?xml.*" + xmlNode +".*" + ")");

        xmlRsFromLogs = findStringInLogs(regexp);

        if (xmlRsFromLogs.equals(null)){
            return false;
        }
        System.out.println("Что в лога RS:" + xmlRqFromLogs);
        return true;
    }

    private String findStringInLogs(Pattern regexp) {

        Matcher m;
        String timeFromLog;
        Scanner sc = null;
        FileInputStream inputStream = null;

        try {

            System.out.println("Зашли в метод поиска по регулярке :" + regexp + "\n");

            inputStream = new FileInputStream(System.getProperty("user.dir") + "\\src\\test\\resources\\logBH\\log1");
            sc = new Scanner(inputStream, "UTF-8");
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                m = regexp.matcher(line);

                if (m.find()) {

                    timeFromLog = m.group(1);

                    if (checkResponseTimeInRange(timeFromLog)) {
                        System.out.println("xml сохранена : " + m.group(2));
                        return m.group(2);

                    }
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (sc != null) {
                sc.close();
            }
        }

        return null;

    }

    protected String getXmlRq(){return xmlRqFromLogs;}
    protected String getXmlRs(){return xmlRsFromLogs;}

    private   Boolean checkResponseTimeInRange(String timeFromLogStr){

        LocalDateTime timeFromLog = LocalDateTime.parse(timeFromLogStr, formatter);

        Boolean flagTime = timeFromLog.isAfter(minRangeTime) && timeFromLog.isBefore(maxRangeTime);

        if (flagTime){

            System.out.println("Дата входит в промежуток между <Минимальной датой> - " + minRangeTime + " и <Максимальной датой> - " + maxRangeTime + " ?? " +  ": " + flagTime);
        }

        return flagTime;
    }

    private void setRangeForParseLog(LocalDateTime responseTime){

        minRangeTime = responseTime.minusSeconds(2);
        maxRangeTime = responseTime.plusSeconds(20);

        day = responseTime.getDayOfMonth();
        month = responseTime.getMonthValue();
        year = responseTime.getYear() - 2000;

        System.out.println("Day : " + day + "\n"
                + "Month : " + month + "\n"
                + "Year : " + year + "\n");

        System.out.println("Задан диапозон с датой, мин. дата : "  + minRangeTime + " и макс. дата : " + maxRangeTime + "\n");

    }


//    public void setXmlRqFromLogs(String xmlRqFromLogs) {
//        this.xmlRqFromLogs = xmlRqFromLogs;
//    }
//
//    public void setXmlRsFromLogs(String xmlRsFromLogs) {
//        this.xmlRsFromLogs = xmlRsFromLogs;
//    }
}
