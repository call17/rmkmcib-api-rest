package utils;


import com.jcraft.jsch.*;
import org.apache.tika.io.IOUtils;


import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by victo on 01.11.2016.
 */
public  class JSchClient {

    private static String user = "testlog";
    private static String hostname = "sbt-oafs-892.ca.sbrf.ru";
    private static String password = "testlog";
    private static Integer port = 22;
    private static String regex =  "SystemOut(.*|.([\\d][\\d].[\\d][\\d].[\\d][\\d])_([\\d][\\d].[\\d][\\d].[\\d][\\d])).log";


    public static void updateLog() {

        String filePathToLogs1 = "/u01/IBM/WebSphere/AppServer/profiles/AppSrv01/logs/server1/";
        String filePathToLogs2 = "/u01/IBM/WebSphere/AppServer/profiles/AppSrv02/logs/server2/";

        try {
            JSch jsch = new JSch();

            Session session = jsch.getSession(user, hostname, port);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword(password);
            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;

            overwriteLogLocal(appendLogsInOne(sftpChannel,filePathToLogs1));
            appendLogLocal(appendLogsInOne(sftpChannel,filePathToLogs2));

            sftpChannel.exit();
            session.disconnect();

        }catch (JSchException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SftpException e) {
            e.printStackTrace();
        }


    }

    public static InputStream appendLogsInOne(ChannelSftp sftpChannel,String filePath) throws IOException, SftpException {

        //Лист потоков, в него собираются все потоки, перед "склейкой"
        List<InputStream> opened = new ArrayList<InputStream>();
        Matcher matcher;
        Pattern regexp = Pattern.compile(regex);

        Vector<ChannelSftp.LsEntry> fileNameList = sftpChannel.ls(filePath);
        System.out.println("Поиск логов в директории: " + filePath);
        for (ChannelSftp.LsEntry entry : fileNameList){

            matcher = regexp.matcher(entry.getFilename());
            if (matcher.find()){

                System.out.println("Найден файл с логами: " + entry.getFilename());

                    opened.add(sftpChannel.get(filePath + entry.getFilename()));

            }

        }

        SequenceInputStream appendsLog = new SequenceInputStream(Collections.enumeration(opened));

        return new SequenceInputStream(Collections.enumeration(opened));
    }

    public static void overwriteLogLocal(InputStream logs) throws IOException {

        File f = new File(System.getProperty("user.dir") + "\\src\\test\\resources\\logBH\\log1");

        FileWriter fileWriter = null;

        fileWriter = new FileWriter(f,false);
        fileWriter.write(IOUtils.toCharArray(logs));

        logs.close();


    }
    public static void appendLogLocal(InputStream logs) throws IOException {

        File f = new File(System.getProperty("user.dir") + "\\src\\test\\resources\\logBH\\log1");

        FileWriter fileWriter = null;

        fileWriter = new FileWriter(f,true);
        fileWriter.write(IOUtils.toCharArray(logs));

        logs.close();

    }




}


