package utils;

import com.jayway.restassured.path.xml.XmlPath;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;
import static com.jayway.restassured.path.xml.XmlPath.from;
import static com.jayway.restassured.matcher.RestAssuredMatchers.matchesXsdInClasspath;
import static org.hamcrest.MatcherAssert.assertThat;
/**
 * Created by victo on 16.10.2016.
 */
public class XmlUtils {

    private static String requestFileDirectory = System.getProperty("user.dir") + "\\src\\test\\resources\\xml\\";
    private static String responseFileDirectory = System.getProperty("user.dir") + "\\src\\test\\resources\\xml\\response\\response";

//
//    public static void main(String[] args) throws FileNotFoundException {
//        XmlUtils xmlUtils = new XmlUtils();
//        XmlPath xmlPath = xmlUtils.parseXml(readFileAsString());
//        System.out.println(xmlPath.getString("..RqUID"));
//
//    }

    public static void validateXml(String xml, String classPath){

        //classPath =xsd\\ + xsdName
        assertThat(xml,matchesXsdInClasspath(classPath));

    }


    public static String getStringValue(String xml,String path){

        return parseXml(xml).getString(path);
    }

    public static void writeResponseToFile(String response, String filenameResponse){

        try{
            Files.write(Paths.get(responseFileDirectory + filenameResponse),response.getBytes());
        }catch (IOException e){
            System.out.println("не удалось записать ответ в файл");
            e.printStackTrace();
        }



    }
    
    public static String readRequestFromFile(String filenameRequest){
        
        File f = new File(requestFileDirectory +  filenameRequest);
        Path path = f.toPath();
        List<String> listXML = null;
        
        try{
            
            listXML = Files.readAllLines(path, Charset.defaultCharset()); 
        
        } catch (IOException e) {
            System.out.println("Невозможно прочитать файл с запросом из директории");
            e.printStackTrace();
        }

        StringBuilder stringBuilder = new StringBuilder();


        for  (String line : listXML ){
            stringBuilder.append(line.trim());
        }

        return stringBuilder.toString();


    }

    public static XmlPath parseXml(String xml){

        XmlPath xmlPath = from(xml);
        return xmlPath;
    }


    public  static void checkSPNameInRq(String xml){

        String expected = "urn:sbrfsystems:99-ufs";
        assertEquals(expected,getStringValue(xml,"..SPName"));
    }

    public static void checkSystemIdInRq(String xml){

        String expected = "urn:sbrfsystems:99-crmorg";
        assertEquals(expected,getStringValue(xml,"..SystemId"));
    }






}
