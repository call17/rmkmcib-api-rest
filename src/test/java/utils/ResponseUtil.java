package utils;


import com.jayway.restassured.response.Response;
import com.jayway.restassured.module.jsv.JsonSchemaValidator;
import org.testng.Assert;


import java.util.LinkedHashMap;
import java.util.List;

import static com.jayway.jsonpath.JsonPath.parse;
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.equalTo;

/**
 * Created by victo on 24.10.2016.
 */
public class ResponseUtil {




    public static String getResponseBody(Response response){
        return response.then().log().all().extract().body().asString();
    }

    public static String checkResponseOnSuccess(Response response) {
        return response.then().statusCode(200).log().all().body("success", equalTo(true)).extract().asString();

    }

    public static String checkResponseOnSuccess(Response response, String schema) {

        return response.then().statusCode(200).log().all().body("success", equalTo(Boolean.TRUE))
                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath(schema)).extract().asString();
    }

    public static String getStringAttributeFromResponse(String response, String jsonPath) {

        return parse(response).read(jsonPath).toString();
    }
    public static List<String> getAttributeArrayFromResponse(String response, String jsonpath){

        List<String> list = parse(response).read(jsonpath);

        return list;

    }

    // TODO: 27.10.2016 как ты работаешь, тварь??
    public static Boolean checkArrayInResponseNotEmpty(String responseBody, String jsonPath){

        Boolean flagEmpty = null;
        List<LinkedHashMap> listArrays = parse(responseBody).read(jsonPath);

        for (LinkedHashMap map: listArrays){

            if (!map.containsValue("")){
                flagEmpty = true;
            }else {
                return false;
            }
        }

        // <----- Если null, не проверяется блок сверху
        return flagEmpty;
    }

    public static Boolean checkAllAttributeInResponse(String responseBody, String jsonPath){

        Boolean flagEmpty = null;
        LinkedHashMap attributeMap = parse(responseBody).read(jsonPath);
        System.out.println(attributeMap);
        if(attributeMap.containsValue("")){
            flagEmpty = true;
        }else{

        }

        // <----- Если null, не проверяется блок сверху
        return flagEmpty;
    }


    // TODO: 14.11.2016 Переделать
    public static Boolean checkStringAtrributeInArrays(String responseBody, String expected,String jsonPath){

        Boolean flagWrongAttribute = null;
        List<String> listAttribute = parse(responseBody).read(jsonPath);

        if (!listAttribute.contains(null)){
            for (String attribute : listAttribute ){

                System.out.println(attribute);
                if (attribute.equals(expected)){
                    flagWrongAttribute = true;
                }else {
                    return false;
                }
            }
        }else {
            return null;
        }

        return flagWrongAttribute;

    }








}
