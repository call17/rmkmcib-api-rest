package utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by sbt-yakimov-vi on 28.10.2016.
 */
public class Utils {

    public static Object[][] readTestDataFromJSON(String fileName) throws FileNotFoundException {
        JsonParser p = new JsonParser();
        JsonArray result = p.parse(readJsonFromFile(fileName)).getAsJsonObject().getAsJsonArray("data");

        Object[][] obj = new Object[result.size()][];
        for (int i = 0; i < result.size(); i++) {
            obj[i] = new Object[1];
            obj[i][0] = result.get(i);
        }
        return obj;

    }

    public static String readJsonFromFile(String fileName) {

        JsonParser jp = new JsonParser();
        String result = null;
        try {
            result = jp
                    .parse(new JsonReader(
                            new InputStreamReader(new FileInputStream(new File(fileName)), StandardCharsets.UTF_8)))
                    .toString();

        } catch (JsonIOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JsonSyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return result;
    }

    public static String readFileAsString(){

        String file = null;
        try {
            file = new String(Files.readAllBytes(Paths.get("C:\\Users\\sbt-yakimov-vi\\Desktop\\rmkmcib-api-rest\\src\\test\\resources\\xml\\temp.xml"))).trim();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(file);

        return file;
    }

    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormat.format(new Date());
    }

}
