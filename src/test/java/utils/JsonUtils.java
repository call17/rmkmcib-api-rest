package utils;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import com.jayway.jsonpath.JsonPath;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;

import static com.jayway.jsonpath.JsonPath.parse;

/**
 * Created by victo on 24.09.2016.
 */
public class JsonUtils {

    private static JsonParser parser = new JsonParser();

    /**
     * Чтение JSON из файла
     *
     * @param filename - путь к файлу
     * @return - JSONObject
     */
    public static JsonObject readJSONObject(String filename) {

        Object obj = null;

        try {
            obj = parser.parse(new FileReader(filename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return (JsonObject) obj;
    }

    public static String getStringValueFromJsonArray(JsonObject json, String memberName, String param) {

        String value = null;
        JsonArray arr = json.getAsJsonArray(memberName);
        Iterator i = arr.iterator();

        while (i.hasNext()) {

            JsonObject jobj = (JsonObject) i.next();
            value = jobj.get(param).toString();
        }

        return value;
    }
    public static String readjsonFromFile(String filename){


        JsonParser jp = new JsonParser();
        String result  = null;
        try{

            result = jp
                    .parse(new JsonReader(new InputStreamReader(new FileInputStream(new File(System.getProperty("user.dir") + filename)), StandardCharsets.UTF_8)))
                            .toString();

        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }

        return result;

    }

//    public static String setValueInJson(String value,String fileName){
//
//        JsonPath jsonPath = parse(readjsonFromFile(fileName)).read();
//
//
//        return null;
//    }

}
