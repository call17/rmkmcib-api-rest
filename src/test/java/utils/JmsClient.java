package utils;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


/**
 * Created by victo on 10.10.2016.
 */
public class JmsClient implements ExceptionListener {


    private String filenameRequest;
    private String filenameRead;
    private static String xmlOutputSirectory = System.getProperty("user.dir") + "\\src\\test\\resources\\xml\\response";
    private static String xmlDirectory = System.getProperty("user.dir") + "\\src\\test\\resources\\xml\\";
    private static String brokerURL = "tcp://0.0.0.0:61616";
    private static String queueIN = "EFS.IN";
    private static String queueOUT = "EFS.OUT";





//    public static void main(String args[]){
//
//        JmsClient jmsClient = new JmsClient();
//        jmsClient.send("GetLegalClientManagerTaskDetailsRequest");
//        jmsClient.read();
//
//    }

    public String send(String filenameRequest) {

        String request = "";

        try {

            this.filenameRequest = filenameRequest;
            // Create a ConnectionFactory
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerURL);

            // Create a Connection
            Connection connection = connectionFactory.createConnection();
            connection.start();

            // Create a Session
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Create the destination (Topic or Queue)
            Destination destination = session.createQueue(queueIN);

            // Create a MessageProducer from the Session to the Topic or Queue
            MessageProducer producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);


            request = XmlUtils.readRequestFromFile(filenameRequest);


            System.out.println("SendMessage:" + request);



            TextMessage message = session.createTextMessage(request);

            producer.send(message);

            session.close();
            connection.close();
        }
        catch (Exception e) {
            System.out.println("Caught: " + e);
            e.printStackTrace();
        }

        return request;
    }

    public String read() {

        String response = "";

        try {

            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerURL);

            Connection connection = connectionFactory.createConnection();
            connection.start();

            connection.setExceptionListener(this);

            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            Destination destination = session.createQueue(queueOUT);

            MessageConsumer consumer = session.createConsumer(destination);

            consumer.getMessageListener();
            Message message ;

            while (true){

                message = consumer.receive(20);
                if (message instanceof TextMessage && !message.equals(null)){
                    TextMessage textMessage = (TextMessage) message;
                    response = textMessage.getText();
                    System.out.println("Ответ получен");
                    break;
                } else {

                }

            }
            XmlUtils.writeResponseToFile(response,filenameRequest);



            consumer.close();
            session.close();
            connection.close();

        } catch (Exception e) {
            System.out.println("Caught: " + e);
            e.printStackTrace();
        }


        return response;
    }


    public void onException(JMSException e) {

    }

}
