package basestep;



import api.RequestList;
import ru.yandex.qatools.allure.annotations.Step;

import static utils.ResponseUtil.*;


/**
 * Created by victo on 01.11.2016.
 */
public class AgentsStep extends LoginStep{

    String responseBody;
    RequestList requestList;

    @Step("Авторизация под клиентским менеджером(Harlamov_IM/123)")
    public void authClientManager(){

        this.requestList = loginAsClientManager();
    }

    @Step("Проверка аттрибутивного состава списка представителей")
    public void getAgentsList(String clientId){

        checkResponseOnSuccess(requestList.getAgentsList(clientId),"\\json-schema-cib\\crm\\agents\\agent.json");

    }

}
