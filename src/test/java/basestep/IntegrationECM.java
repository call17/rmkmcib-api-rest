package basestep;

import com.google.gson.JsonObject;
import com.jayway.restassured.response.ValidatableResponse;
import api.RequestList;
import utils.JsonUtils;

import static org.hamcrest.Matchers.equalTo;

/**
 * Created by sbt-yakimov-vi on 27.09.2016.
 */
public class IntegrationECM {

    private RequestList requestList;
    private JsonObject userData = JsonUtils.readJSONObject("src/test/resources/users.json");
    private JsonObject knowlegdeBaseRequest = JsonUtils.readJSONObject("src/test/resources/ecm/knowledgeBaseRequest.json");
    private JsonObject contentData = JsonUtils.readJSONObject("src/test/resources/ecm/content.json");



    public void setUp(){
        requestList = new RequestList();
    }


    public void loginTest(){


        ValidatableResponse response =  requestList

                .login( userData.get("username").toString(),
                        userData.get("password").toString())
                .then()
                .statusCode(200)
                .log().all()
                .body("success", equalTo(true))
                .body("error", equalTo(null))
                .body("body", equalTo("ok"))
                ;

        requestList.setCookie(response.extract().cookie("UFS-SESSION"),response.extract().cookie("JSESSIONID"));

    }


    public void getKnowledgeBaseTreeTest(){

        ValidatableResponse response = requestList

                .getTreeFromKnowledgeBase(knowlegdeBaseRequest)
                .then()
                .log().body()
                .statusCode(200);
//                .body("error", null);
    }

//    @Test(priority = 3)
//    public void getFetchContentTest(){
//
//        ValidatableResponse response = rmkmcib_api
//
//                .getFetchContent(contentData.get("index").toString(),
//                                 contentData.get("target").toString())
//                .then()
//                .log().all();
//
//    }









}
