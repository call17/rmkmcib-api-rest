package basestep;

import api.RequestList;
import com.jayway.restassured.response.Response;

import static utils.ResponseUtil.*;

/**
 * Created by victo on 24.10.2016.
 */
public class LoginStep {

    RequestList requestList;

//    @BeforeClass
//    public void setUp(){
//
//        requestList = new RequestList();
//    }
    // TODO: 27.10.2016 Добавить тест-данные по всем ролям из классификатора + проерка ролей 4.19.3, для проверки в тесте получения списка клиентов

    public RequestList loginAsDefaultUser(){

        requestList = new RequestList();

        Response response =
                requestList.login("user1","1111")
                ;
        checkResponseOnSuccess(response);
        requestList.setCookie(response.getCookie("UFS-SESSION"),response.getCookie("JSESSIONID"));
        return requestList;

    }

    public RequestList loginAsClientManager(){

        requestList = new RequestList();

        Response response =
                requestList.login("Harlamov_IM","123")
                ;
        checkResponseOnSuccess(response);
        requestList.setCookie(response.getCookie("UFS-SESSION"),response.getCookie("JSESSIONID"));
        return requestList;

    }

}
