package basestep;

import api.RequestList;
import com.google.gson.JsonObject;
import com.jayway.restassured.response.Response;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import utils.JsonUtils;
import utils.LogsParser;


import static org.testng.Assert.assertEquals;
import static utils.XmlUtils.*;
import static org.testng.Assert.assertTrue;
import static utils.ResponseUtil.*;

/**
 * Created by sbt-yakimov-vi on 21.10.2016.
 */
@Features("Тестирование рест сервисов бх")
@Stories("Тестирование редактирования задачи")
public class TasksStep extends LogsParser{

    private Response responseR;
    private String response;
    private RequestList requestList;
    private JsonObject userData = JsonUtils.readJSONObject("src/test/resources/users.json");



    // TODO: 07.11.2016 Вынести проверки ответа в отдельный степ?
    @Step("Отправлен рест запрос получения списка задач по КМ")
    public void sendRequestTaskList(Integer page){

        responseR = requestList.getTaskList(page);
    }

    @Step("Отправлен рест запрос получения  карточки задачи")
    public void sendRequestTaskCard(String taskId){

        responseR = requestList.getClientCard(taskId);
    }



    @Step("Проверен овтет от рест-запроса")
    public void checkResponse(){

        checkResponseOnSuccess(responseR);

    }

    @Step("Авторизация под клиентским менеджером(Harlamov_IM/123)")
    public void authClientManager(){

        requestList = new RequestList();

        Response response =
                requestList.login("Harlamov_IM","123")
                ;
        checkResponseOnSuccess(response);
        requestList.setCookie(response.getCookie("UFS-SESSION"),response.getCookie("JSESSIONID"));
    }


    @Step("Наличие заданного атрибута Списка Задач(валидация по схеме)")
    public void checkAttributeTaskList(Integer page){

        response = checkResponseOnSuccess(requestList.getTaskList(page),"TaskListSchema.json");
        checkArrayInResponseNotEmpty(response,"$.body.tasks");



    }
    @Step("Наличие заданного списка атрибутов карточки задачи(валидация по схеме)")
    public void checkAttributeInTaskCard(String taskId){

        response = checkResponseOnSuccess(requestList.getDetailedTask(taskId),"\\json-schema-cib\\crm\\tasks\\_tasks.json");

        checkAllAttributeInResponse(response,"$.body.task");
    }

    @Step("Создание новой задачи ")
    public String createTask(String json){

        response = checkResponseOnSuccess(requestList.createTask(json));

        //получение айди новой задачи

        return getStringAttributeFromResponse(response,"$.body.task.id");

    }

    @Step("Получение и проверка задачи")
    public void getTask(String idTask){

        checkResponseOnSuccess(requestList.getDetailedTask(idTask),"DetailedTaskSchema.json");

    }

    @Step("Проверка, что созданная/отредактировання задача вернулась в списке")
    public void checkTaskListContainsTaskWithId(String idCreatedTask){

        assertTrue(checkAttributeContainsInAllTaskList(idCreatedTask),"Новой задачи нету в списке задач");

    }
    @Step
    public String updateTask(String json,String idNewTask){


        response = checkResponseOnSuccess(requestList.updateTask(json),"DetailedTaskSchema.json");
        // TODO: 14.11.2016 Скоммуниздить метод по изменению значения в json
        return getStringAttributeFromResponse(response,"$.body.task.id");
    }

    @Step("Проверка, что логи бх содержат запись о запросе")
    public void checkRqInLog(String xmlNode){

        //Получили время запроса из поля класса RequestList
        setResponseTime(requestList.getRequestTime());

        assertTrue(checkRqInLogs(xmlNode),"Не найден запрос в логах");
    }

    @Step("Проверка, что логи бх содержат запись о ответе")
    public void checkRsInLog(String xmlNode){


        assertTrue(checkRsInLogs(xmlNode),"Не найден ответ в логах");
    }

    @Step("Валидация xml запроса")
    public void validateRq(String xsdSchemaClassPath){

        validateXml(getXmlRq(),xsdSchemaClassPath);
    }

    @Step("Валидация xml ответа")
    public void validateRs(String xsdSchemaClassPath){

        validateXml(getXmlRs(),xsdSchemaClassPath);
    }

    @Step("Проверка SPName в xml ")
    public void checkSPNameInReq(){

        checkSPNameInRq(getXmlRq());
    }
    @Step("Проверка SystemId в xml ")
    public void checkSystemIdInReq(){

        checkSystemIdInRq(getXmlRq());
    }
    @Step("Проверка UserLogin в xml ")
    public void checkUserLoginInRq(){

        String expected = "sbt-ivanov-ii";
        assertEquals(expected,getStringValue(getXmlRq(),"..UserLogin"));
    }

    @Step("Проверка метода в запросе")
    public void checkMethodInRq(){

        String expected = "MRMCIBGetActions";
        assertEquals(expected,getStringValue(getXmlRq(),"..Method"));
    }







    //"Проверка, что созданная задача вернулась в спике задач"
    public Boolean checkAttributeContainsInAllTaskList(String idCreatedTask){

        Boolean flagContains = false;

        for (int i = 1; i < Integer.MAX_VALUE; i ++){

            response = checkResponseOnSuccess(requestList.getTaskList(i),"TaskList.json");
            if (checkStringAtrributeInArrays(response,idCreatedTask,"$.body.tasks.id")){
                flagContains = true;
                break;
            }
            if (getStringAttributeFromResponse(response,"$.body.tasks.isLast").equals(Boolean.TRUE)){

                break;
            }

        }

        return flagContains;

    }




}
