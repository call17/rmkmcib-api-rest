package basestep;

import api.RequestList;
import com.google.gson.JsonObject;
import com.jayway.restassured.response.Response;
import ru.yandex.qatools.allure.annotations.Step;
import utils.JsonUtils;
import utils.LogsParser;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static utils.ResponseUtil.*;
import static utils.XmlUtils.*;


/**
 * Created by sbt-yakimov-vi on 18.10.2016.
 */

//TK 4.10.7

public class ProductsStep extends LogsParser{

    private Response responseR;
    private JsonObject userData = JsonUtils.readJSONObject("src/test/resources/users.json");
    private  RequestList requestList;
    private String responseBodyRKO;
    private String responseDepositBody;
    private String responseEncashmentBody;
    private String responseBodyCorporateCard;
    private String responseCreditsBody;




    @Step("Авторизация под user1/1111")
    public void auth(){

        requestList = new RequestList();

        Response response =
                requestList.login("Harlamov_IM","123")
                ;
        checkResponseOnSuccess(response);
        requestList.setCookie(response.getCookie("UFS-SESSION"),response.getCookie("JSESSIONID"));
    }

    @Step("Отправка запроса на получение РКО,customPayments,cashManagementsList")
    public void sendRequestRKO(String orgId){

        requestList.getRKObyById(orgId);
    }

    @Step("Отправка запроса на получение депозитов,НСО,СДО. ")
    public void sendRequestDepositNsoSdo(String orgId){

        requestList.getDeposits(orgId);
    }

    @Step("Отпарвка запроса на получение Кредитовби т.д. ")
    public void sendRequestCredits(String orgId){

        requestList.getCredits(orgId);
    }

    @Step("Отправка запроса на получение Корпоративных  карт и т.д.")
    public void sendRequestCorporateCard(String orgId){

        requestList.getCorporateCard(orgId);
    }

    @Step("Отправка запроса на полчуение Acquiring")
    public void sendRequestAcquiring(String orgId){

        requestList.getCorporateCard(orgId);
    }

    @Step("Проверка, что логи бх содержат запись о запросе")
    public void checkRqInLog(String xmlNode){

        //Получили время запроса из поля класса RequestList
        setResponseTime(requestList.getRequestTime());

        assertTrue(checkRqInLogs(xmlNode),"Не найден запрос в логах");
    }

    @Step("Проверка, что логи бх содержат запись о ответе")
    public void checkRsInLog(String xmlNode){


        assertTrue(checkRsInLogs(xmlNode),"Не найден ответ в логах");
    }

    @Step("Валидация xml запроса")
    public void validateRq(String xsdSchemaClassPath){

        validateXml(getXmlRq(),xsdSchemaClassPath);
    }

    @Step("Валидация xml ответа")
    public void validateRs(String xsdSchemaClassPath){

        validateXml(getXmlRs(),xsdSchemaClassPath);
    }
    @Step("Проверка orgNum в запросе")
    public void checkOrgNum(String orgId){


        assertEquals(orgId,getStringValue(getXmlRq(),"..OrgNum"));
    }
//    @Step("Проверка UserLogin в xml ")
//    public void checkUserLoginInRq(){
//
//        String expected = "sbt-ivanov-ii";
//        assertEquals(expected,getStringValue(getXmlRq(),"..UserLogin"));
//    }





    @Step("Валидация ответа по схеме - CustomPayments,TariffPlans,CashManagements")
    public void checkRKO(String orgId){

        responseBodyRKO =
        checkResponseOnSuccess(
                requestList.getRKObyById(orgId),
                "SchemaRKO.json"
        );
    }


    @Step("Проверка отсутствия пустых значений в CustomPayments,TariffPlans,CashManagements ")
    public void checkAttributeRKO(){

        assertTrue(checkArrayInResponseNotEmpty(responseBodyRKO,"$.body.paymentAccounts"));
        assertTrue(checkArrayInResponseNotEmpty(responseBodyRKO,"$.body.customsPayments"));
        assertTrue(checkArrayInResponseNotEmpty(responseBodyRKO,"$.body.cashManagements"));

    }

    @Step("Валидация ответа по схеме - Deposits")
    public void checkDeposits(String orgId){

        responseDepositBody =
                checkResponseOnSuccess(
                        requestList.getDeposits(orgId),
                        "DepositsSchema.json"
                );
    }

    //TODO contractNSO, contractSDO
    @Step("Проверка отсутствия пустых значений в Deposits")
    public void checkAttributeDeposits(){

        assertTrue(checkArrayInResponseNotEmpty(responseDepositBody,"$.body.deposits"));
//        assertTrue(checkArrayInResponseNotEmpty(responseDepositBody,"$..contractNSO"));
//        assertTrue(checkArrayInResponseNotEmpty(responseDepositBody,"$..contractSDO"));

    }
//    @Step("Проверка отсутствия пустых значений в NSO,SDO")
//    public void checkAttributeNSOaandSDO(){
//
//
//        assertTrue(checkArrayInResponseNotEmpty(responseDepositBody,"$.body.contractNSO"));
//        assertTrue(checkArrayInResponseNotEmpty(responseDepositBody,"$.body.contractSDO"));
//
//    }

    @Step("Валидация ответа по схеме - Encashment, selfEncashment")
    public void checkEncashment(String orgId){

        responseEncashmentBody =
                checkResponseOnSuccess(
                        requestList.getEncashment(orgId),
                        "Encashment.json"
                );
    }

    @Step("Проверка отсутствия пустых значений в Encashment, selfEncashment")
    public void checkAttributeEncashments(){

        assertTrue(checkArrayInResponseNotEmpty(responseEncashmentBody,"$.body.encashments"));
        assertTrue(checkArrayInResponseNotEmpty(responseEncashmentBody,"$.body.selfEncashments"));

    }

    @Step("Валидация ответа по схеме - CorporateCards")
    public void checkCorporateCards(String orgId){

        responseBodyCorporateCard =
                checkResponseOnSuccess(
                        requestList.getCorporateCard(orgId)
                );

    }

    @Step("Проверка отсутствия пустых значений в CorporateCard")
    public void checkAttributeCorporateCards(){

        assertTrue(checkArrayInResponseNotEmpty(responseBodyCorporateCard,"$.body.corporateCards"));

    }

    @Step("Валидация ответа по схеме - Credits,BankGarantees")
    public void checkCredits (String orgId){

        responseCreditsBody =
                checkResponseOnSuccess(
                        requestList.getCredits(orgId),
                        "Credits.json"
                );

    }

    @Step("Проверка отсутствия пустых значений в Credits,bankGarantees")
    public void checkAttributeCredits(){

        assertTrue(checkArrayInResponseNotEmpty(responseCreditsBody,"$.body.credits"));
        assertTrue(checkArrayInResponseNotEmpty(responseCreditsBody,"$.body.bankGuarantees"));

    }


    @Step("Проверка SPName в xml ")
    public void checkSPNameInReq(){

        checkSPNameInRq(getXmlRq());
    }
    @Step("Проверка SystemId в xml ")
    public void checkSystemIdInReq(){

        checkSystemIdInRq(getXmlRq());
    }












}
