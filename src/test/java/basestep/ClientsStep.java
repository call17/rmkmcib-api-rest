package basestep;

import api.RequestList;
import com.jayway.restassured.response.Response;
import ru.yandex.qatools.allure.annotations.Step;
import utils.LogsParser;

import java.time.LocalDateTime;
import java.util.List;

import static org.testng.Assert.*;
import static utils.ResponseUtil.*;
import static com.jayway.jsonpath.JsonPath.parse;
import static utils.XmlUtils.*;

/**
 * Created by sbt-yakimov-vi on 27.10.2016.
 */
public class ClientsStep extends LogsParser {

    private String responseBody;
    private RequestList requestList;
    private LocalDateTime responseTime;
    private String requestXml;
    private Response response;




    @Step("Отправка запроса получения карточки клиента")
    protected void sendRequestClientCard(String clientId){

        response = requestList.getClientCard(clientId);

    }

    @Step("Отправка запроса на полчение списка клиентов")
    protected void sendRequestClientList(String page,String pageSize){

        response = requestList.getClientList(page,pageSize);
    }


    @Step("Авторизация под клиентским менеджером(testcrmmanafer1/12321)")
    protected void authClientManager(){

        requestList = new RequestList();

        Response response =
                requestList.login("testcrmmanager1","12321")
                ;
        checkResponseOnSuccess(response);
        requestList.setCookie(response.getCookie("UFS-SESSION"),response.getCookie("JSESSIONID"));

    }

    @Step("Получение списка клиентов(Валидация по схеме, проверка что атрибуты не пустые)")
    protected void checkAttributeInClientList(String page, String pageSize){

                String responseBody =
                        checkResponseOnSuccess(
                            requestList.getClientList(page,pageSize) ,
                            "ClientListSchema.json"
                        );
                assertTrue(checkArrayInResponseNotEmpty(responseBody,"$.body.clients"));
    }

    //// TODO: 27.10.2016 Иправить на все роли, пока скелетон с логикой(вроде норм)
    @Step("Проверка роли под дефолтным юзером")
    protected void checkClientListWithBadAuth(String page, String pageSize){

        requestList = new RequestList();

        Response response =
                requestList.login("user1","1111")
                ;
        checkResponseOnSuccess(response);
        requestList.setCookie(response.getCookie("UFS-SESSION"),response.getCookie("JSESSIONID"));
        responseBody = getResponseBody(requestList.getClientList(page,pageSize));
        checkClientListNull(responseBody,"$.body");

    }

    @Step("Проверка роли в списке клиенте под клиентским менеджером")
    protected void checkRoleWIthAuthClientManager(String page, String pageSize, String expectedRole){

        requestList = new RequestList();

        Response response =
                requestList.login("Harlamov_IM","123")
                ;
        checkResponseOnSuccess(response);
        requestList.setCookie(response.getCookie("UFS-SESSION"),response.getCookie("JSESSIONID"));

        responseBody = checkResponseOnSuccess(requestList.getClientList(page,pageSize));
        // TODO: 14.11.2016 Разобраться
        assertTrue(checkStringAtrributeInArrays(responseBody,expectedRole,"$..role"));

    }

    @Step("Проверка атрибутивного состава карточки клиента(валидация по схеме)")
    protected void checkAttributeClientCard(String clientId){

        Response response = requestList.getClientCard(clientId);
        String clientCardType = getStringAttributeFromResponse(response.then().extract().body().asString(),"$.body.clientCard.orgType");
        // TODO: Разные ли json схемы?
        if (clientCardType == "Holding"){
            checkResponseOnSuccess(response, "ClientCardHoldingSchema.json");
        }else {
            checkResponseOnSuccess(response, "ClientCardHoldingSchema.json");
        }

        // TODO: 28.10.2016 Добавить проверку на пустую строку
        checkAllAttributeInResponse(getResponseBody(response),"$.body.clientCard");
    }


    @Step("Проверка, что список клиентов содержит список иерархий")
    protected   void checkIerarchyContainsInClientCard(String page, String pageSize){

        List<String> listIdClientCard = getListIdClients(checkResponseOnSuccess(requestList.getClientList(page,pageSize)));
        for (String clientId: listIdClientCard){

            assertTrue(checkStringAtrributeInArrays(checkResponseOnSuccess(requestList.getClientCard(clientId)),"123","$.body.hierarchy"),"Пустая иерархия у клиента" + clientId);
        }
    }


    @Step("Проверка наличия запроса в логах бх")
    protected void checkLogRq(String xmlNode){

        setResponseTime(requestList.getRequestTime());

        assertTrue(checkRqInLogs(xmlNode),"В логах отсутствует запрос");
    }

    @Step("Проверка наличия ответа в логах бх")
    public void checkLogRs(String xmlNode){

        assertTrue(checkRsInLogs(xmlNode),"В логах отсутствует ответ");
    }

    @Step("Валидация запроса по схеме")
    public void validateRq(String xsdSchemaClassPath){

        validateXml(getXmlRq(),xsdSchemaClassPath);
    }

    @Step("Валидация ответа по схеме")
    public void validateRs(String xsdSchemaClassPath){

        validateXml(getXmlRs(),xsdSchemaClassPath);
    }

    @Step("Проверка SPName в xml ")
    public void checkSPNameInReq(){

        checkSPNameInRq(getXmlRq());
    }
    @Step("Проверка SystemId в xml ")
    public void checkSystemIdInReq(){

        checkSystemIdInRq(getXmlRq());
    }

    @Step("Проверка UserLogin в xml ")
    public void checkUserLoginInRq(String expectedUserLogin){

        assertEquals(expectedUserLogin,getStringValue(getXmlRq(),"..UserLogin"));
    }

    @Step("Проверка метода в запросе")
    public void checkMethodInRq(String expectedMethodName){

        assertEquals(expectedMethodName,getStringValue(getXmlRq(),"..Method"));
    }

    @Step("Проверка ответа от рест сервиса Получения Списка Клиентов")
    protected void checkResponseClientList(){
        checkResponseOnSuccess(response);

    }

    @Step("Проверка ответа от рест сервиса Получения Карточки Клиента")
    protected void checkResponseClientCard(){
        checkResponseOnSuccess(response);

    }


    //Helperochki
    public static void checkClientListNull(String responseBody, String jsonPath){

        assertNull(parse(responseBody).read(jsonPath));
    }

    public List<String> getListIdClients(String response){

        // TODO: 14.11.2016 Разобраться
        return getAttributeArrayFromResponse(response,"$..id");
    }





}
