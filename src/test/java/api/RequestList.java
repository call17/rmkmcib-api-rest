package api;

import com.google.gson.JsonObject;
import com.jayway.restassured.RestAssured;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;


import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

import static com.jayway.restassured.RestAssured.given;


/**
 * Created by victo on 24.09.2016.
 */
public class RequestList {


    private String cookieUFS;
    private String cookieJSession;
    private Long responseTimeMillis;

    public RequestList(){

        RestAssured.baseURI = "http://sbt-oafs-892.ca.sbrf.ru:9080/km-cib-app";
//        RestAssured.baseURI = "http://163.172.179.156:32124/km-cib-bh-war";
//        RestAssured.baseURI = "http://localhost:8800";
        RestAssured.basePath = "/rs";

    }

    public String getCookieUFS(){return cookieUFS;}
    public String getCookieJSession(){return cookieJSession;}

    public void setCookie(String cookieUFS, String cookieJSession ){
        this.cookieJSession = cookieJSession;
        this.cookieUFS = cookieUFS;
    }

    public  Response login(String username, String password){

            Response response =
                    given()
                        .contentType("application/json")
                        .body("{\n" +
                                "\t\"username\":\"" + username + "\",\n" +
                                "\t\"password\":\"" + password + "\"\n" +
                                "}")
                            .log().all()
                        .post("/login")
                    ;



        return response;
    }

    public Response getClientList(String page,String pageSize){

        responseTimeMillis = System.currentTimeMillis();
        Response response =
                given()
                        .queryParam("page",page)
                        .queryParam("pageSize",pageSize)
                        .cookie("UFS-SESSION",cookieUFS)
                        .cookie("JSESSIONID",cookieJSession)
                        .log().all()
                .get("/clients")
                ;

        return response;
    }

    public Response getClientCard(String clientId){

        responseTimeMillis = System.currentTimeMillis();
        Response response =
                given()
                        .pathParameter("id",clientId )
                        .cookie("UFS-SESSION",cookieUFS)
                        .cookie("JSESSIONID",cookieJSession)
                .log().all()
                .get("clients/{id}");

        return  response;
    }

    public Response getDetailedTask(String id){

        responseTimeMillis = System.currentTimeMillis();
        Response response =
                given()
                        .pathParam("id", id)
                        .cookie("UFS-SESSION", cookieUFS)
                        .cookie("JSESSIONID",cookieJSession)
                        .log().all()
                .get("/tasks/{id}");

        return response;
    }

    public Response getTaskList(Integer page){


        responseTimeMillis = System.currentTimeMillis();
        Response response =
                given()

                        .queryParam("page", page)
//                        .queryParam("pageSize", pageSize)
                        .cookie("UFS-SESSION",cookieUFS)
                        .cookie("JSESSIONID",cookieJSession)
                        .log().all()
                .get("/tasks/");

        return response;
    }



    public Response getTreeFromKnowledgeBase(JsonObject jsonRequest){

        responseTimeMillis = System.currentTimeMillis();
        Response response =
                given()
                        .contentType(ContentType.JSON)
                        .body(jsonRequest)
                        .cookie("UFS-SESSION",cookieUFS)
                        .cookie("JSESSIONID",cookieJSession)
                        .log().all()
                .post("/filebase/tree");

        return response;
    }

//    public Response getFetchContent(String index, String target){
//
//        Response response =
//                given()
//                        .multiPart("file1", new File("/home/johan/some_large_file.bin"))
//                        .queryParam("index",index)
//                        .queryParam("target", target)
//                        .log().all()
//                        .get("/filebase/content")
//
//                ;
//
//
//
//        return response;
//    }

    public Response getRKObyById(String orgId){

        responseTimeMillis = System.currentTimeMillis();
        Response response =
                given()
                .pathParam("orgId",orgId)
                        .cookie("UFS-SESSION",cookieUFS)
                        .cookie("JSESSIONID",cookieJSession)
                        .log().all()
                .get("clients/{orgId}/products/settlementAgreement")
                ;

        return response;
    }

    public Response getDeposits(String orgId){

        responseTimeMillis = System.currentTimeMillis();
        Response response =
                given()
                .pathParam("orgId", orgId)
                        .cookie("UFS-SESSION",cookieUFS)
                        .cookie("JSESSIONID",cookieJSession)
                .log().all()
                .get("clients/{orgId}/products/deposits")
                ;

        return response;
    }

    public Response getCredits(String orgId){

        responseTimeMillis = System.currentTimeMillis();
        Response response =
                given()
                .pathParam("orgId", orgId)
                        .cookie("UFS-SESSION",cookieUFS)
                        .cookie("JSESSIONID",cookieJSession)
                .log().all()
                .get("clients/{orgId}/products/credits")
                ;

        return response;
    }

    public Response getCorporateCard(String orgId){

        responseTimeMillis = System.currentTimeMillis();
        Response response =
                given()
                        .pathParam("orgId", orgId)
                        .cookie("UFS-SESSION",cookieUFS)
                        .cookie("JSESSIONID",cookieJSession)
                        .log().all()
                        .get("clients/{orgId}/products/corporateCards")
                ;

        return  response;

    }
    public Response getEncashment(String orgId){

        responseTimeMillis = System.currentTimeMillis();
        Response response =
                given()
                        .pathParam("orgId", orgId)
                        .cookie("UFS-SESSION",cookieUFS)
                        .cookie("JSESSIONID",cookieJSession)
                        .log().all()
                        .get("clients/{orgId}/products/encashmentContracts")
                ;

        return response;

    }

    public Response updateTask(String json){

        responseTimeMillis = System.currentTimeMillis();
        Response response =

                given()
                        .contentType("application/json")
                        .body(json)
                        .cookie("UFS-SESSION",cookieUFS)
                        .cookie("JSESSIONID",cookieJSession)
                        .log().all()
                        .put("/tasks/")
                ;

        return response;

    }

    public Response createTask(String json){

        responseTimeMillis = System.currentTimeMillis();
        Response response =

                given()
                .contentType("application/json")
                .body(json)
                        .cookie("UFS-SESSION",cookieUFS)
                        .cookie("JSESSIONID",cookieJSession)
                        .log().all()
                .post("/tasks")
                ;

        return  response;
    }

    public Response getAgentsList(String clientId){

        responseTimeMillis = System.currentTimeMillis();
        Response response =

                given()
                        .pathParam("clientid",clientId)
                        .cookie("UFS-SESSION",cookieUFS)
                        .cookie("JSESSIONID",cookieJSession)
                        .log().all()
                        .get("agents/{clientid}")
                ;

        return response;
    }

    public String getCurrentDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormat.format(new Date());
    }

    public LocalDateTime getRequestTime(){

        LocalDateTime responseTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(responseTimeMillis), ZoneId.systemDefault());
        return responseTime;
    }


}
